package com.sdsmdg.tastytoast;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * Created by Anas Altair on 8/31/2016.
 * Modified by rahul on 16/09/2016
 */
public class ConfusingToastView extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    PixelMap eye;
    AnimatorValue valueAnimator;
    float angle = 0f;
    private Paint mPaint;
    private float mWidth = 0f;
    private float mHeight = 0f;

    public ConfusingToastView(Context context) {
        super(context);
        init();
    }

    public ConfusingToastView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public ConfusingToastView(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init();
    }

    public ConfusingToastView(Context context, AttrSet attrs, int resId) {
        super(context, attrs, resId);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        mWidth = EstimateSpec.getSize(widthEstimateConfig);
        mHeight = EstimateSpec.getSize(heightEstimateConfig);
        initPaint();
        initPath();
        return false;
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new Color(Color.getIntColor("#FE9D4D")));
    }

    private void initPath() {
        Path mPath = new Path();
        RectFloat rectF = new RectFloat(mWidth / 2f - dip2px(1.5f), mHeight / 2f - dip2px(1.5f)
                , mWidth / 2f + dip2px(1.5f), mHeight / 2f + dip2px(1.5f));
        mPath.addArc(rectF, 180f, 180f);
        rectF.modify(rectF.left - dip2px(3), rectF.top - dip2px(1.5f), rectF.right, rectF.bottom + dip2px(1.5f));
        mPath.addArc(rectF, 0f, 180f);
        rectF.modify(rectF.left, rectF.top - dip2px(1.5f), rectF.right + dip2px(3), rectF.bottom + dip2px(1.5f));
        mPath.addArc(rectF, 180f, 180f);
        rectF.modify(rectF.left - dip2px(3), rectF.top - dip2px(1.5f), rectF.right, rectF.bottom + dip2px(1.5f));
        mPath.addArc(rectF, 0f, 180f);

        PixelMap.InitializationOptions option = new PixelMap.InitializationOptions();
        option.size = new Size((int) mWidth, (int) mHeight);
        option.pixelFormat = PixelFormat.ARGB_8888;
        eye = PixelMap.create(option);
        Canvas c = new Canvas(new Texture(eye));
        mPaint.setStrokeWidth(dip2px(1.7f));
        c.drawPath(mPath, mPaint);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.save();
        canvas.rotate(angle, mWidth / 4f, mHeight * 2f / 5f);
        Size size = eye.getImageInfo().size;
        PixelMapHolder holder = new PixelMapHolder(eye);
        canvas.drawPixelMapHolder(holder,
                mWidth / 4f - (size.width / 2f), mHeight * 2f / 5f - (size.height / 2f), mPaint);
        canvas.restore();
        canvas.save();
        canvas.rotate(angle, mWidth * 3f / 4f, mHeight * 2f / 5f);
        canvas.drawPixelMapHolder(holder,
                mWidth * 3f / 4f - (size.width / 2f), mHeight * 2f / 5f - (size.height / 2f), mPaint);
        canvas.restore();

        mPaint.setStrokeWidth(dip2px(2f));
        canvas.drawLine(new Point(mWidth / 4f, mHeight * 3f / 4f), new Point(
                mWidth * 3f / 4f, mHeight * 3f / 4f), mPaint);
    }

    public float dip2px(float dpValue) {
        return AttrHelper.vp2px(dpValue, getContext());
    }

    public void startAnim() {
        stopAnim();
        startViewAnim(0f, 1f, 2000);
    }

    public void stopAnim() {
        if (valueAnimator != null) {
            valueAnimator.end();
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    private AnimatorValue startViewAnim(float startF, final float endF, long time) {
        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(time);
        valueAnimator.setCurveType(Animator.CurveType.LINEAR);
        valueAnimator.setLoopedCount(AnimatorValue.INFINITE);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                angle += 4;
                getContext().getUITaskDispatcher().asyncDispatch(ConfusingToastView.this::invalidate);
            }
        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();

        }
        return valueAnimator;
    }
}