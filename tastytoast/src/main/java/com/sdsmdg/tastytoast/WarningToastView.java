package com.sdsmdg.tastytoast;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by rahul on 25/7/16.
 */
public class WarningToastView extends Component implements Component.EstimateSizeListener, Component.DrawTask {
    AnimatorValue valueAnimator;
    RectFloat rectFOne = new RectFloat();
    RectFloat rectFTwo = new RectFloat();
    RectFloat rectFThree = new RectFloat();
    private Paint mPaint;
    private float mWidth = 0f;
    private float mHeight = 0f;
    private float mStrokeWidth = 0f;
    private float mPadding = 0f;
    private float mPaddingBottom = 0f;
    private float endAngle = 0f;

    public WarningToastView(Context context) {
        super(context);
        init();
    }


    public WarningToastView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public WarningToastView(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init();
    }

    public WarningToastView(Context context, AttrSet attrs, int resId) {
        super(context, attrs, resId);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        initPaint();
        initRect();
        mHeight = EstimateSpec.getSize(widthEstimateConfig);
        mWidth = EstimateSpec.getSize(heightEstimateConfig);
        mPadding = convertDpToPixel(2);
        mPaddingBottom = mPadding * 2;
        mStrokeWidth = convertDpToPixel(2);
        return false;
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new Color(Color.getIntColor("#f0ad4e")));
        mPaint.setStrokeWidth(mStrokeWidth);
    }

    private void initRect() {
        rectFOne = new RectFloat(mPadding, 0, mWidth - mPadding, mWidth - mPaddingBottom);
        rectFTwo = new RectFloat((float) (1.5 * mPadding), convertDpToPixel(6) + mPadding +
                mHeight / 3, mPadding + convertDpToPixel(9), convertDpToPixel(6) + mPadding + mHeight / 2);
        rectFThree = new RectFloat(mPadding + convertDpToPixel(9), convertDpToPixel(3) + mPadding +
                mHeight / 3, mPadding + convertDpToPixel(18), convertDpToPixel(3) + mPadding + mHeight / 2);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        canvas.drawArc(rectFOne, new Arc(170, -144, false), mPaint);
        canvas.drawLine(new Point(mWidth - convertDpToPixel(3) - mStrokeWidth, (float) (mPadding +
                mHeight / 6)), new Point(mWidth - convertDpToPixel(3) - mStrokeWidth,
                mHeight - convertDpToPixel(2) - mHeight / 4), mPaint);

        canvas.drawLine(new Point(mWidth - convertDpToPixel(3) - mStrokeWidth - convertDpToPixel(8), (float) (mPadding +
                mHeight / 8.5)), new Point(mWidth - convertDpToPixel(3) - mStrokeWidth - convertDpToPixel(8),
                (float) (mHeight - convertDpToPixel(3) - mHeight / 2.5)), mPaint);

        canvas.drawLine(new Point(
                mWidth - convertDpToPixel(3) - mStrokeWidth - convertDpToPixel(17), (float) (mPadding +
                mHeight / 10)), new Point(mWidth - convertDpToPixel(3) - mStrokeWidth - convertDpToPixel(17),
                (float) (mHeight - convertDpToPixel(3) - mHeight / 2.5)), mPaint);

        canvas.drawLine(new Point(
                mWidth - convertDpToPixel(3) - mStrokeWidth - convertDpToPixel(26), (float) (mPadding +
                mHeight / 10)), new Point(mWidth - convertDpToPixel(3) - mStrokeWidth - convertDpToPixel(26),
                (float) (mHeight - convertDpToPixel(2) - mHeight / 2.5)), mPaint);

        canvas.drawArc(rectFTwo, new Arc(170, 180, false), mPaint);
        canvas.drawArc(rectFThree, new Arc(175, -150, false), mPaint);
    }

    public float convertDpToPixel(float dp) {
        return AttrHelper.vp2px(dp, getContext());
    }
	
    public void startAnim() {
        stopAnim();
        startViewAnim(0f, 0.7f, 2000);
    }

    public void stopAnim() {
        if (valueAnimator != null) {
            valueAnimator.end();
        }
    }

    private AnimatorValue startViewAnim(float startF, final float endF, long time) {
        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(time);
        valueAnimator.setCurveType(Animator.CurveType.SPRING);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                v = v * (endF - startF) + startF;
                WarningToastView.this.setScaleX(v);
                WarningToastView.this.setScaleY(v);
                getContext().getUITaskDispatcher().asyncDispatch(WarningToastView.this::invalidate);
            }
        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();
        }
        return valueAnimator;
    }
}