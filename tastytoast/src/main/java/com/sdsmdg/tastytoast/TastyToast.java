package com.sdsmdg.tastytoast;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * Created by rahul on 28/7/16.
 */
public class TastyToast {
    public static final int LENGTH_SHORT = 2000;
    public static final int LENGTH_LONG = 3500;


    public static final int SUCCESS = 1;
    public static final int WARNING = 2;
    public static final int ERROR = 3;
    public static final int INFO = 4;
    public static final int DEFAULT = 5;
    public static final int CONFUSING = 6;

    static SuccessToastView successToastView;
    static WarningToastView warningToastView;
    static ErrorToastView errorToastView;
    static InfoToastView infoToastView;
    static DefaultToastView defaultToastView;
    static ConfusingToastView confusingToastView;

    public static ToastDialog makeText(Context context, String msg, int length, int type) {

        ToastDialog toast = new ToastDialog(context);

        switch (type) {
            case SUCCESS: {
                Component layout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_success_toast_layout
                        , null, false);
                Text text = (Text) layout.findComponentById(ResourceTable.Id_toastMessage);
                text.setText(msg);
                successToastView = (SuccessToastView) layout.findComponentById(ResourceTable.Id_successView);
                successToastView.startAnim();
                text.setBackground(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_success_toast));
                modifyRoundRect(text);
                text.setTextColor(new Color(Color.getIntColor("#FFFFFF")));
                toast.setComponent(layout);
                break;
            }
            case WARNING: {
                Component layout = LayoutScatter.getInstance(context).parse(ResourceTable
                        .Layout_warning_toast_layout, null, false);

                Text text = (Text) layout.findComponentById(ResourceTable.Id_toastMessage);
                text.setText(msg);

                warningToastView = (WarningToastView) layout.findComponentById(ResourceTable.Id_warningView);
                warningToastView.startAnim();
                text.setBackground(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_warning_toast));
                modifyRoundRect(text);
                text.setTextColor(new Color(Color.getIntColor("#FFFFFF")));
                toast.setComponent(layout);
                break;
            }
            case ERROR: {
                Component layout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_error_toast_layout,
                        null, false);

                Text text = (Text) layout.findComponentById(ResourceTable.Id_toastMessage);
                text.setText(msg);
                errorToastView = (ErrorToastView) layout.findComponentById(ResourceTable.Id_errorView);
                errorToastView.startAnim();
                text.setBackground(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_error_toast));
                modifyRoundRect(text);
                text.setTextColor(new Color(Color.getIntColor("#FFFFFF")));
                toast.setComponent(layout);
                break;
            }
            case INFO: {
                Component layout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_info_toast_layout,
                        null, false);

                Text text = (Text) layout.findComponentById(ResourceTable.Id_toastMessage);
                text.setText(msg);
                infoToastView = (InfoToastView) layout.findComponentById(ResourceTable.Id_infoView);
                infoToastView.startAnim();
                text.setBackground(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_info_toast));
                modifyRoundRect(text);
                text.setTextColor(new Color(Color.getIntColor("#FFFFFF")));
                toast.setComponent(layout);
                break;
            }
            case DEFAULT: {
                Component layout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_toast_layout
                        , null, false);

                Text text = (Text) layout.findComponentById(ResourceTable.Id_toastMessage);
                text.setText(msg);
                defaultToastView = (DefaultToastView) layout.findComponentById(ResourceTable.Id_defaultView);
                defaultToastView.startAnim();
                text.setBackground(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_default_toast));
                modifyRoundRect(text);
                text.setTextColor(new Color(Color.getIntColor("#FFFFFF")));
                toast.setComponent(layout);
                break;
            }
            case CONFUSING: {
                Component layout =
                        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_confusing_toast_layout, null,
                                false);

                Text text = (Text) layout.findComponentById(ResourceTable.Id_toastMessage);
                text.setText(msg);
                confusingToastView = (ConfusingToastView) layout.findComponentById(ResourceTable.Id_confusingView);
                confusingToastView.startAnim();
                text.setBackground(ElementScatter.getInstance(context).parse(ResourceTable.Graphic_confusing_toast));
                modifyRoundRect(text);
                text.setTextColor(new Color(Color.getIntColor("#FFFFFF")));
                toast.setComponent(layout);
                break;
            }
        }
        toast.registerDisplayCallback(new BaseDialog.DisplayCallback() {
            @Override
            public void onDisplay(IDialog iDialog) {
                if (toast.getComponent() != null && toast.getComponent().getComponentParent() != null) {
                    ((Component) toast.getComponent().getComponentParent()).setBackground(null);
                }
            }
        });
        toast.setCornerRadius(30);
        toast.setDuration(length);
        toast.show();
        return toast;
    }

    private static void modifyRoundRect(Component component) {
        if (component != null && component.getBackgroundElement() instanceof ShapeElement) {
            ShapeElement background = (ShapeElement) component.getBackgroundElement();
            int radiusPx = AttrHelper.vp2px(background.getCornerRadius(), component.getContext());
            background.setCornerRadiiArray(new float[]{0, 0, radiusPx, radiusPx, radiusPx, radiusPx, 0, 0});
        }
    }
}
