package com.sdsmdg.tastytoast;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

/**
 * Created by rahul on 27/7/16.
 */
public class DefaultToastView extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    AnimatorValue valueAnimator;
    float mAnimatedValue = 0f;
    private Paint mPaint, mSpikePaint;
    private float mWidth = 0f;
    private float mPadding = 0f;
    private float mSpikeLength;


    public DefaultToastView(Context context) {
        super(context);
        init();
    }

    public DefaultToastView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public DefaultToastView(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init();
    }

    public DefaultToastView(Context context, AttrSet attrs, int resId) {
        super(context, attrs, resId);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        initPaint();
        mWidth = EstimateSpec.getSize(widthEstimateConfig);
        mPadding = dip2px(5);
        return false;
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new Color(Color.getIntColor("#222222")));
        mPaint.setStrokeWidth(dip2px(2));

        mSpikePaint = new Paint();
        mSpikePaint.setAntiAlias(true);
        mSpikePaint.setStyle(Paint.Style.STROKE_STYLE);
        mSpikePaint.setColor(new Color(Color.getIntColor("#222222")));
        mSpikePaint.setStrokeWidth(dip2px(4));

        mSpikeLength = dip2px(4);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.save();
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 4, mPaint);

        for (int i = 0; i < 360; i += 40) {

            int angle = (int) (mAnimatedValue * 40 + i);
            float initialX = (float) ((mWidth / 4) * Math.cos(angle * Math.PI / 180));
            float initialY = (float) ((mWidth / 4) * Math.sin(angle * Math.PI / 180));
            float finalX = (float) ((mWidth / 4 + mSpikeLength) * Math.cos(angle * Math.PI / 180));
            float finalY = (float) ((mWidth / 4 + mSpikeLength) * Math.sin(angle * Math.PI / 180));
            canvas.drawLine(new Point(mWidth / 2 - initialX, mWidth / 2 - initialY), new Point(mWidth / 2 - finalX,
                    mWidth / 2 - finalY), mSpikePaint);
        }
        canvas.restore();
    }


    public void startAnim() {
        stopAnim();
        startViewAnim(0f, 1f, 2000);
    }

    public void stopAnim() {
        if (valueAnimator != null) {
            valueAnimator.end();
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    private AnimatorValue startViewAnim(float startF, final float endF, long time) {
        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(time);
        valueAnimator.setCurveType(Animator.CurveType.LINEAR);
        valueAnimator.setLoopedCount(AnimatorValue.INFINITE);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                v = v * (endF - startF) + startF;
                mAnimatedValue = v;
                getContext().getUITaskDispatcher().asyncDispatch(DefaultToastView.this::invalidate);
            }
        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();

        }
        return valueAnimator;
    }

    public int dip2px(float dpValue) {
        return AttrHelper.vp2px(dpValue, getContext());
    }
}
