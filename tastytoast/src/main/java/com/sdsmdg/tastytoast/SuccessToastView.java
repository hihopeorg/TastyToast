package com.sdsmdg.tastytoast;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by rahul on 22/7/16.
 */
public class SuccessToastView extends Component implements Component.EstimateSizeListener, Component.DrawTask {
    RectFloat rectF = new RectFloat();
    AnimatorValue valueAnimator;
    float mAnimatedValue = 0f;
    private Paint mPaint;
    private float mWidth = 0f;
    private float mEyeWidth = 0f;
    private float mPadding = 0f;
    private float endAngle = 0f;
    private boolean isSmileLeft = false;
    private boolean isSmileRight = false;

    public SuccessToastView(Context context) {
        super(context);
        init();
    }

    public SuccessToastView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public SuccessToastView(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init();
    }

    public SuccessToastView(Context context, AttrSet attrs, int resId) {
        super(context, attrs, resId);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        initPaint();
        initRect();
        mWidth = EstimateSpec.getSize(widthEstimateConfig);
        mPadding = dip2px(10);
        mEyeWidth = dip2px(3);
        return false;
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new Color(Color.getIntColor("#5cb85c")));
        mPaint.setStrokeWidth(dip2px(2));
    }

    private void initRect() {
        rectF = new RectFloat(mPadding, mPadding, mWidth - mPadding, mWidth - mPadding);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        canvas.drawArc(rectF, new Arc(180, endAngle, false), mPaint);

        mPaint.setStyle(Paint.Style.FILL_STYLE);
        if (isSmileLeft) {
            canvas.drawCircle(mPadding + mEyeWidth + mEyeWidth / 2, mWidth / 3, mEyeWidth, mPaint);
        }
        if (isSmileRight) {
            canvas.drawCircle(mWidth - mPadding - mEyeWidth - mEyeWidth / 2, mWidth / 3, mEyeWidth, mPaint);
        }
    }

    public int dip2px(float dpValue) {
        return AttrHelper.vp2px(dpValue, getContext());
    }

    public void startAnim() {
        stopAnim();
        startViewAnim(0f, 1f, 2000);
    }

    public void stopAnim() {
        if (valueAnimator != null) {
            isSmileLeft = false;
            isSmileRight = false;
            mAnimatedValue = 0f;
            valueAnimator.end();
        }
    }

    private AnimatorValue startViewAnim(float startF, final float endF, long time) {
        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(time);
        valueAnimator.setCurveType(Animator.CurveType.LINEAR);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                v = v * (endF - startF) + startF;
                mAnimatedValue = v;
                if (mAnimatedValue < 0.5) {
                    isSmileLeft = false;
                    isSmileRight = false;
                    endAngle = -360 * (mAnimatedValue);
                } else if (mAnimatedValue > 0.55 && mAnimatedValue < 0.7) {
                    endAngle = -180;
                    isSmileLeft = true;
                    isSmileRight = false;
                } else {
                    endAngle = -180;
                    isSmileLeft = true;
                    isSmileRight = true;
                }

                getContext().getUITaskDispatcher().asyncDispatch(SuccessToastView.this::invalidate);
            }
        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();

        }
        return valueAnimator;
    }
}