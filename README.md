# TastyToast

**本项目是基于开源项目TastyToast进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/yadav-rahul/TastyToast ）追踪到原项目版本**

#### 项目介绍

- 项目名称：TastyToast自定义Toast弹框
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一组自定义Toast弹框控件
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/yadav-rahul/TastyToast
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示

<img src="gif/TastyToast1.gif"/>

<img src="gif/TastyToast2.gif"/>

<img src="gif/TastyToast3.gif"/>

<img src="gif/TastyToast4.gif"/>

<img src="gif/TastyToast5.gif"/>

<img src="gif/TastyToast6.gif"/>

#### 安装教程

方法1.

1. 下载tastytoast三方库har包tastytoast.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'tastytoast', ext: 'har')
	……
}
```

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.sdsmdg.tastytoast.ohos:tastytoast:1.0.1'
}
```

#### 使用说明

```
TastyToast.makeText(getContext(), "Hello World !", TastyToast.LENGTH_LONG, TastyToast.WARNING);
```
最后一个参数为想要显示的Toast风格种类.

#### 版本迭代

- v1.0.1

#### 版权和许可信息

```
Copyright 2016 Rahul Yadav

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

