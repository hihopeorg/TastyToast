package com.sdsmdg.demoexample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import com.sdsmdg.tastytoast.TastyToast;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_button).setClickedListener(this::showSuccessToast);
        findComponentById(ResourceTable.Id_button2).setClickedListener(this::showWarningToast);
        findComponentById(ResourceTable.Id_button3).setClickedListener(this::showErrorToast);
        findComponentById(ResourceTable.Id_button4).setClickedListener(this::showInfoToast);
        findComponentById(ResourceTable.Id_button5).setClickedListener(this::showDefaultToast);
        findComponentById(ResourceTable.Id_button6).setClickedListener(this::showConfusingToast);
    }

    public void showSuccessToast(Component view) {
        TastyToast.makeText(getApplicationContext(), "Download Successful !", TastyToast.LENGTH_LONG,
                TastyToast.SUCCESS);
    }

    public void showWarningToast(Component view) {
        TastyToast.makeText(getApplicationContext(), "Are you sure ?", TastyToast.LENGTH_LONG,
                TastyToast.WARNING);
    }

    public void showErrorToast(Component view) {
        TastyToast.makeText(getApplicationContext(), "Downloading failed ! Try again later ", TastyToast.LENGTH_LONG,
                TastyToast.ERROR);
    }
    public void showInfoToast(Component view) {
        TastyToast.makeText(getApplicationContext(), "Searching for username : 'Rahul' ", TastyToast.LENGTH_LONG,
                TastyToast.INFO);
    }

    public void showDefaultToast(Component view) {
        TastyToast.makeText(getApplicationContext(), "This is Default Toast", TastyToast.LENGTH_LONG,
                TastyToast.DEFAULT);
    }

    public void showConfusingToast(Component view) {
        TastyToast.makeText(getApplicationContext(), "I don't Know !", TastyToast.LENGTH_LONG,
                TastyToast.CONFUSING);
    }
}
