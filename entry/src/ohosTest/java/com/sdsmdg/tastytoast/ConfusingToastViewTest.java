package com.sdsmdg.tastytoast;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.animation.AnimatorValue;
import ohos.app.Context;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;
public class ConfusingToastViewTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private ConfusingToastView view = new ConfusingToastView(context);
    @Test
    public void onEstimateSize() {
        assertEquals(false,view.onEstimateSize(7,8));

    }

    @Test
    public void dip2px() {
        assertEquals(27,view.dip2px(9),0.0);
    }

    @Test
    public void startAnim() throws NoSuchFieldException, IllegalAccessException {
        view.startAnim();
        Field field = ConfusingToastView.class.getDeclaredField("valueAnimator");
        Object fieldValue = field.get(view);
        assertEquals(true,((AnimatorValue)fieldValue).isRunning());
    }

    @Test
    public void stopAnim() throws NoSuchFieldException, IllegalAccessException {
        view.startAnim();
        view.stopAnim();
        Field field = ConfusingToastView.class.getDeclaredField("valueAnimator");
        Object fieldValue = field.get(view);
        assertEquals(false,((AnimatorValue)fieldValue).isRunning());
    }
}