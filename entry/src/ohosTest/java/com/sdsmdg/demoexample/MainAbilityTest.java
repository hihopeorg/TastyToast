package com.sdsmdg.demoexample;

import com.sdsmdg.tastytoast.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.lang.reflect.Field;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private static Ability ability=EventHelper.startAbility(MainAbility.class);
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01214, "TastyToastTest");
    private void stopThread(int x){
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void test01DownLoad(){
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button);
        SuccessToastView successToastView =null;

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        stopThread(500);
        try {
            Field f = TastyToast.class.getDeclaredField("successToastView");
            f.setAccessible(true);
           successToastView = (SuccessToastView)f.get(null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }
        Text text =(Text) ((DirectionalLayout) successToastView.getComponentParent().getComponentParent()).findComponentById(com.sdsmdg.tastytoast.ResourceTable.Id_toastMessage);
        ShapeElement backgroundElement = (ShapeElement)text.getBackgroundElement();
        ShapeElement parse = (ShapeElement) ElementScatter.getInstance(ability.getApplicationContext()).parse(com.sdsmdg.tastytoast.ResourceTable.Graphic_success_toast);
        AnimatorValue valueAnimator=null;
        try {
            Field f = SuccessToastView.class.getDeclaredField("valueAnimator");
            f.setAccessible(true);
            valueAnimator = (AnimatorValue)f.get(successToastView);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("文字部分显示异常",backgroundElement.getRgbColors()[0].equals(parse.getRgbColors()[0])&&text.getText().equals("Download Successful !"));
        Assert.assertTrue("动画未运行",valueAnimator.isRunning()==true);
        stopThread(3000);

    }
    @Test
    public void test02Warning(){
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button2);
        WarningToastView warningToastView =null;

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        stopThread(500);
        try {
            Field f = TastyToast.class.getDeclaredField("warningToastView");
            f.setAccessible(true);
            warningToastView = (WarningToastView)f.get(null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Text text =(Text) ((DirectionalLayout) warningToastView.getComponentParent().getComponentParent()).findComponentById(com.sdsmdg.tastytoast.ResourceTable.Id_toastMessage);
        ShapeElement backgroundElement = (ShapeElement)text.getBackgroundElement();
        ShapeElement parse = (ShapeElement) ElementScatter.getInstance(ability.getApplicationContext()).parse(com.sdsmdg.tastytoast.ResourceTable.Graphic_warning_toast);

        AnimatorValue valueAnimator=null;
        try {
            Field f = WarningToastView.class.getDeclaredField("valueAnimator");
            f.setAccessible(true);
            valueAnimator = (AnimatorValue)f.get(warningToastView);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("文字部分显示异常",backgroundElement.getRgbColors()[0].equals(parse.getRgbColors()[0])&&text.getText().equals("Are you sure ?"));
        Assert.assertTrue("动画未运行",valueAnimator.isRunning()==true);
        stopThread(3000);

    }
    @Test
    public void test03Error(){
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button3);
        ErrorToastView errorToastView =null;

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        stopThread(500);
        try {
            Field f = TastyToast.class.getDeclaredField("errorToastView");
            f.setAccessible(true);
            errorToastView = (ErrorToastView)f.get(null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Text text =(Text) ((DirectionalLayout) errorToastView.getComponentParent().getComponentParent()).findComponentById(com.sdsmdg.tastytoast.ResourceTable.Id_toastMessage);
        ShapeElement backgroundElement = (ShapeElement)text.getBackgroundElement();
        ShapeElement parse = (ShapeElement) ElementScatter.getInstance(ability.getApplicationContext()).parse(com.sdsmdg.tastytoast.ResourceTable.Graphic_error_toast);

        AnimatorValue valueAnimator=null;
        try {
            Field f = ErrorToastView.class.getDeclaredField("valueAnimator");
            f.setAccessible(true);
            valueAnimator = (AnimatorValue)f.get(errorToastView);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("文字部分显示异常",backgroundElement.getRgbColors()[0].equals(parse.getRgbColors()[0])&&text.getText().equals("Downloading failed ! Try again later "));
        Assert.assertTrue("动画未运行",valueAnimator.isRunning()==true);
        stopThread(3000);

    }
    @Test
    public void test04Info(){
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button4);
        InfoToastView infoToastView =null;

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        stopThread(500);
        try {
            Field f = TastyToast.class.getDeclaredField("infoToastView");
            f.setAccessible(true);
            infoToastView = (InfoToastView)f.get(null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Text text =(Text) ((DirectionalLayout) infoToastView.getComponentParent().getComponentParent()).findComponentById(com.sdsmdg.tastytoast.ResourceTable.Id_toastMessage);
        ShapeElement backgroundElement = (ShapeElement)text.getBackgroundElement();
        ShapeElement parse = (ShapeElement) ElementScatter.getInstance(ability.getApplicationContext()).parse(com.sdsmdg.tastytoast.ResourceTable.Graphic_info_toast);

        AnimatorValue valueAnimator=null;
        try {
            Field f = InfoToastView.class.getDeclaredField("valueAnimator");
            f.setAccessible(true);
            valueAnimator = (AnimatorValue)f.get(infoToastView);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("文字部分显示异常",backgroundElement.getRgbColors()[0].equals(parse.getRgbColors()[0])&&text.getText().equals("Searching for username : 'Rahul' "));
        Assert.assertTrue("动画未运行",valueAnimator.isRunning()==true);
        stopThread(3000);

    }
    @Test
    public void test05Default(){
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button5);
        DefaultToastView defaultToastView =null;

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        stopThread(500);
        try {
            Field f = TastyToast.class.getDeclaredField("defaultToastView");
            f.setAccessible(true);
            defaultToastView = (DefaultToastView)f.get(null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Text text =(Text) ((DirectionalLayout) defaultToastView.getComponentParent().getComponentParent()).findComponentById(com.sdsmdg.tastytoast.ResourceTable.Id_toastMessage);
        ShapeElement backgroundElement = (ShapeElement)text.getBackgroundElement();
        ShapeElement parse = (ShapeElement) ElementScatter.getInstance(ability.getApplicationContext()).parse(com.sdsmdg.tastytoast.ResourceTable.Graphic_default_toast);

        AnimatorValue valueAnimator=null;
        try {
            Field f = DefaultToastView.class.getDeclaredField("valueAnimator");
            f.setAccessible(true);
            valueAnimator = (AnimatorValue)f.get(defaultToastView);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("文字部分显示异常",backgroundElement.getRgbColors()[0].equals(parse.getRgbColors()[0])&&text.getText().equals("This is Default Toast"));
        Assert.assertTrue("动画未运行",valueAnimator.isRunning()==true);
        stopThread(3000);

    }
    @Test
    public void test06Confusion(){
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button6);
        ConfusingToastView confusingToastView =null;

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        stopThread(500);
        try {
            Field f = TastyToast.class.getDeclaredField("confusingToastView");
            f.setAccessible(true);
            confusingToastView = (ConfusingToastView)f.get(null);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Text text =(Text) ((DirectionalLayout) confusingToastView.getComponentParent().getComponentParent()).findComponentById(com.sdsmdg.tastytoast.ResourceTable.Id_toastMessage);
        ShapeElement backgroundElement = (ShapeElement)text.getBackgroundElement();
        ShapeElement parse = (ShapeElement) ElementScatter.getInstance(ability.getApplicationContext()).parse(com.sdsmdg.tastytoast.ResourceTable.Graphic_confusing_toast);

        AnimatorValue valueAnimator=null;
        try {
            Field f = ConfusingToastView.class.getDeclaredField("valueAnimator");
            f.setAccessible(true);
            valueAnimator = (AnimatorValue)f.get(confusingToastView);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("文字部分显示异常",backgroundElement.getRgbColors()[0].equals(parse.getRgbColors()[0])&&text.getText().equals("I don't Know !"));
        Assert.assertTrue("动画未运行",valueAnimator.isRunning()==true);
        stopThread(3000);

    }

}